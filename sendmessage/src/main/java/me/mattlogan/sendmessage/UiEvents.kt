package me.mattlogan.sendmessage

data class StartRecordingEvent(val filePath: String)

object StopRecordingEvent
